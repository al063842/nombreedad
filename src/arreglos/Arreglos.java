/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author Rodrigo Balan
 */
public class Arreglos {

   
    public static void main(String[] args) {
        
        String[] nombreAlum; 
        nombreAlum=new String[5];
        nombreAlum[0]="Rodrigo";
        nombreAlum[1]="Josue";
        nombreAlum[2]="Daniel";
        nombreAlum[3]="Juan";
        nombreAlum[4]="Orlando";
        
        System.out.println("Nombres");
        for (String nombre : nombreAlum){
            System.out.println(nombre);
        }
        
        int[] edadAlum; 
        edadAlum=new int[5];
        edadAlum[0]=19;
        edadAlum[1]=19;
        edadAlum[2]=19;
        edadAlum[3]=19;
        edadAlum[4]=19;
        
        System.out.println("Edades");
        for (int edad : edadAlum){
            System.out.println(edad);
        }
    }
    
}
